package interfaces.printers;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class DecoratePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println("*****\n" + text + "\n*****\n");
    }
}
