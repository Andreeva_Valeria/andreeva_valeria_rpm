package interfaces.main;

import interfaces.printers.AdvConsolePrinter;
import interfaces.printers.ConsolePrinter;
import interfaces.printers.DecoratePrinter;
import interfaces.printers.IPrinter;
import interfaces.readers.FileReader;
import interfaces.readers.IReader;
import interfaces.readers.PredefinedReader;
import interfaces.readers.ScannerText;
import interfaces.replacer.Replacer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        IReader reader = new ScannerText();
        IReader readerFile = new FileReader();
        IPrinter printer = new ConsolePrinter();
        IPrinter decoratePrint = new DecoratePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, decoratePrint);
        Replacer replacerFile = new Replacer(readerFile, decoratePrint);
        replacer.replace();
        advReplacer.replace();
        replacerFile.replace();

    }
}
