package book;

/**
 * Класс для представления книги
 *
 * @author Andreeva V.A.
 */
public class Book {
    private String author;
    private String titleOfBook;
    private int yearOfPublication;
    private Genre genre;
    private String url;

    Book(String author, String titleOfBook, int yearOfPublication, Genre genre, String url) {
        this.author = author;
        this.titleOfBook = titleOfBook;
        this.yearOfPublication = yearOfPublication;
        this.genre = genre;
        this.url = url;
    }

    String getAuthor() {
        return author;
    }

    String getTitleOfBook() {
        return titleOfBook;
    }

    int getYearOfPublication() {
        return yearOfPublication;
    }

    public Genre getGenre() {
        return genre;
    }

    String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Книга {" +
                "автор ='" + author + '\'' +
                ", название книги ='" + titleOfBook + '\'' +
                ", год выпуска =" + yearOfPublication +
                ", жанр =" + genre +
                ", url ='" + url + '\'' +
                '}';
    }
}
