package book;

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;

/**
 * Класс, показывающий возможности реализации объектов класса Book
 *
 * @author Andreeva V.A
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Book book1 = new Book("Пушкин", "Пиковая дама", 1820, Genre.HORROR, "С:/Users/book/Пушкин");
        Book book2 = new Book("Гоголь", "Мертвые души", 1810, Genre.FANTASY, "С:/Users/book/Гоголь");
        Book book3 = new Book("Гончаров", "Обломов", 1850, Genre.NOVEL, "С:/Users/book/Гончаров");
        Book book4 = new Book("Тургенев", "Отцы и дети", 1830, Genre.NOVEL, "С:/Users/book/Тургенев");
        Book book5 = new Book("Пушкин", "Евгений Онегин", 1835, Genre.NOVEL, "С:/Users/book/Пушкин");

        List<Book> books = Arrays.asList(book1, book2, book3, book4, book5);

        print(books);

        System.out.println("Отсортированный список книг по алфавиту по фамилиям авторов: ");
        print(authorSorting(books));

        System.out.println("Отсортированный список книг по году издания от новинок к более  старым изданиям: ");
        print(yearOfPublicationSorting(books));
        
        filtrBookGenre(books);
        

        searchBook(books);
    }

    private static void filtrBookGenre(List<Book> books) {
    }

    /**
     * Выводит адрес книги, найденной по введенному пользователем названию
     *
     * @param books List объектов класса Book
     */
    private static void searchBook(List<Book> books) {
        System.out.print("Введите название книги, которую хотите найти: ");
        String name = scanner.nextLine();
        for (Book book : books) {
            if (name.equalsIgnoreCase(book.getTitleOfBook())) {
                System.out.println("Ваша книга находится по адресу: " + book.getUrl());
            } else {
                System.out.println("Такой книги нет");
                break;
            }
        }
    }

    /**
     * Выводит сведения о каждом объекте в List'e
     *
     * @param books List объектов класса Book
     */
    private static void print(List<Book> books) {
        for (Book book : books) {
            System.out.println(book);
        }
        System.out.println();
    }

    /**
     * Возвращает отсортированный список книг по алфавиту по фамилиям авторов
     *
     * @param books List объектов класса Book
     * @return отсортированный список книг по алфавиту по фамилиям авторовList объектов класса Book
     */
    private static List<Book> authorSorting(List<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = 1 + i; j < books.size(); j++) {
                int compare = books.get(j).getAuthor().compareTo(books.get(i).getAuthor());
                if (compare < 0) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
            }
        }
        return books;
    }


    /**
     * Возвращает отсортированный список книг по году издания от новинок к более  старым изданиям
     *
     * @param books List объектов класса Book
     * @return отсортированный список книг по году издания от новинок к более  старым изданиям
     */
    private static List yearOfPublicationSorting(List<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = i + 1; j < books.size(); j++) {
                if (books.get(j).getYearOfPublication() > books.get(i).getYearOfPublication()) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
            }
        }
        return books;
    }
}

