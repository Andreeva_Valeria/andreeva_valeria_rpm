package book;

/**
 * Класс перечисления жанров книг
 *
 * @author Andreeva V.A.
 */
public enum Genre {
    HORROR,
    COMEDY,
    DRAMA,
    FANTASY,
    NOVEL
}
