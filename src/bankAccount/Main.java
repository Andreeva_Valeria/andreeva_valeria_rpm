package bankAccount;

import java.util.Scanner;

/**
 * Класс демонстрации возможностей банковского аккаунта
 *
 * @author Andreeva V.A.
 */
public class Main implements Message {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        final Account sberbankAccount = new Account("1234234534564567", "Valeria Andreeva", 2500);
        final Account vtbAccount = new Account("2541625389546", "Valeria Andreeva", 3785);
        final Account.Card sberMastercard = sberbankAccount.new Card("2340 7845 8956 8906");
        final Account.Card sberWORLD = sberbankAccount.new Card("3540 2516 2013 1169");
        final Account.Card vtbCard = vtbAccount.new Card("7456 2130 5489 6799");

        System.out.println("Добрый день!");
        int answerAboutContinue;
        do {
            Account chosenAccount = chooseBankAccount(sberbankAccount, vtbAccount);             //выбор аккаунта

            Account.Card chosenCard = vtbCard;                                                  //выбор карты
            if (chosenAccount.equals(sberbankAccount)) {
                chosenCard = chooseCard(sberMastercard, sberWORLD);
            }

            int generalChoice;
            do {
                generalChoice = generalChoose(chosenAccount, chosenCard);
            } while (generalChoice != 3);

            answerAboutContinue = input(MESSAGE_ABOUT_CONTINUE);
        }
        while (answerAboutContinue == 1);
        System.out.println("Спасибо за визит! Досвидания!");
    }

    /**
     * Возвращает целое число,введенное с консоли, в ответ на сообщение message
     *
     * @param message сообщения пользователю
     * @return целое число
     */
    private static int input(String message) {
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     * Возвращает выбранный пользователем банковский аккаунт
     *
     * @param sberbankAccount аккаунт Сбербанка
     * @param vtbAccount      аккаунт ВТБ
     * @return выбранный банковский аккаунт
     */
    private static Account chooseBankAccount(Account sberbankAccount, Account vtbAccount) {
        int answer = input(MESSAGE_ABOUT_ACCOUNT);
        if (answer != 1) {
            return vtbAccount;
        }
        return sberbankAccount;
    }

    /**
     * Возвращает выбранную пользователем карту Аккаунта Сбербанка
     *
     * @param sberMastercard карта Сбербанка MASTERCARD
     * @param sberWORLD      карта Сбербанка МИР
     * @return выбранную карту
     */
    private static Account.Card chooseCard(Account.Card sberMastercard, Account.Card sberWORLD) {
        int answer = input(MESSAGE_ABOUT_CARD);
        if (answer == 1) {
            return sberMastercard;
        }
        return sberWORLD;
    }

    /**
     * Возвращает целое число 3, если пользователь выбрал "Завершить обслуживание данного аккаунта",
     * иначе выполняет действия в зависимости от цифры ответа пользователя (1-выводит баланс;2-вызывает метод chooseOperation)
     * и возвращает соответсвующее число операции для проверки на завершение обслуживания
     *
     * @param chosenAccount выбранный пользователем аккаунт для получения доступа к балансу
     * @param chosenCard    выбранная пользователем карта, с которой будут проводиться операции
     * @return целое число 3, если пользователь выбрал "Завершить обслуживание данного аккаунта"
     */
    private static int generalChoose(Account chosenAccount, Account.Card chosenCard) {
        int generalChoice = input(MESSAGE_ABOUT_GENERAL_CHOICE);
        switch (generalChoice) {
            case 1:
                System.out.println("Баланс аккаунта пользователя:" + chosenAccount.getOwner() + " с номером " + chosenAccount.getNumber() + " составляет " + chosenAccount.getAmount() + " руб. " + "\n");
                return 1;
            case 2:
                chooseOperation(chosenCard);
                return 2;
        }
        return 3;
    }

    /**
     * Позволяет выбрать операцию и реализовать ее посредством вызова одного из методов:
     * withdrawChoice(если выбрано списание со счета) or depositChoice(если выбрано пополнить счет)
     *
     * @param chosenCard выбранная пользователем карта, с которой будут проводиться операции
     */
    private static void chooseOperation(Account.Card chosenCard) {
        int chooseOperation = input(MESSAGE_ABOUT_CHOICE_OPERATION);
        if (chooseOperation == 1) {
            int amountToWithdraw = input(MESSAGE_ABOUT_WITHDRAW);
            withdrawChoice(chosenCard, amountToWithdraw);
        } else {
            int amountToDeposit = input(MESSAGE_ABOUT_DEPOSIT);
            depositChoice(chosenCard, amountToDeposit);
        }
    }

    /**
     * Организует проверку корректности введенных данных для пополнения, если все корректно, возвращает результат операции, иначе сообщает о некорректности
     *
     * @param chosenCard      выбранная карта, с которой будет проводиться операция
     * @param amountToDeposit сумма для пополнения
     */
    private static void depositChoice(Account.Card chosenCard, int amountToDeposit) {
        int result = chosenCard.deposit(amountToDeposit);
        if (result == 0) {
            System.out.println("Некорректные данные\n");
        }
    }

    /**
     * Организует проверку корректности введенных данных для списания, если все корректно, возвращает результат операции, иначе сообщает о некорректности
     *
     * @param chosenCard       выбранная карта, с которой будет проводиться операция
     * @param amountToWithdraw сумма списания
     */
    private static void withdrawChoice(Account.Card chosenCard, int amountToWithdraw) {
        int result = chosenCard.withdraw(amountToWithdraw);
        if (result == 0) {
            System.out.println("Некорректные данные\n");
        }
        if (result < amountToWithdraw) {
            System.out.println("Введенная сумма превышает баланс карты\n");
        }
    }
}

