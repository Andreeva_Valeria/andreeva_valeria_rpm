package bankAccount;

/**
 * Класс для инициализации объекта Account
 *
 * @author Andreeva V.A.
 */
@SuppressWarnings("unused")
class Account {
    private final String number;
    private final String owner;
    private int amount;

    Account(final String number, final String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    String getNumber() {
        return number;
    }

    String getOwner() {
        return owner;
    }

    int getAmount() {
        return amount;
    }

    /**
     * Возвращает сумму снятия
     *
     * @param amountToWithdraw сумма для снятия
     * @return сумму снятия
     */
    private int withdraw(int amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    /**
     * Возвращает сумму пополнения
     *
     * @param amountToDeposit сумма для пополнения
     * @return сумму пополнения
     */
    private int deposit(int amountToDeposit) {
        if (amountToDeposit <= 0) {
            return 0;
        }
        amount = amount + amountToDeposit;
        return amountToDeposit;
    }

    /**
     * Внутренний класс для инициализации объекта Card
     */
    class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        String getNumber() {
            return number;
        }

        int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        int deposit(final int amountToDeposit) {
            return Account.this.deposit(amountToDeposit);
        }
    }
}
