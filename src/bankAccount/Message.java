package bankAccount;

public interface Message {
    String MESSAGE_ABOUT_GENERAL_CHOICE = "1)Запросить баланс\n2)Выбрать операцию\n3)Завершить обслуживание этого аккаунта\nВведите число: ";
    String MESSAGE_ABOUT_ACCOUNT = "Какой аккаунт использовать?\n 1(Сбербанк) or 2(ВТБ). Введите число: ";
    String MESSAGE_ABOUT_CARD = "С какой картой аккаунта 'Сбербанк' вы хотите проводить операции? \n1)MASTERCARD\n2)МИР\nВведите число: ";
    String MESSAGE_ABOUT_CONTINUE = "Продолжить выполнения операций?(1-да/2-нет).Напишите цифру: ";
    String MESSAGE_ABOUT_CHOICE_OPERATION = "Выберите действие:\n1)Снять деньги\n2)Положить деньги\nВведите число: ";
    String MESSAGE_ABOUT_DEPOSIT = "Какую сумму хотите положить?";
    String MESSAGE_ABOUT_WITHDRAW = "Какую сумму хотите снять?";
}
