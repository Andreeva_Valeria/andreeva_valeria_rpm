package obfuscator.firstObfuscate;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс, реализующий мини-обфускатор
 *
 * @author Andreeva V.A.
 */
public class FirstObfuscate {
    public static void main(String[] args) throws IOException {
        String soursPath = "D:\\work\\cod.java";
        String targetPath;

        String listLine = fileReader(soursPath);
        String multiLine = deleteComments(deleteGamps(listLine));
        targetPath = getFileName(soursPath);
        String newFilePath = newFilePath(soursPath, targetPath);
        fileRecorder(multiLine, newFilePath);
        printOut(listLine);
    }

    /**
     * Возвращает считанный из файоа код в виде мультистроки
     *
     * @param soursPath путь к файлу
     * @return считанный из файоа код в виде мультистроки
     */
    private static String fileReader(String soursPath) {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get(soursPath));
            for (String s : list) {
                listLine.append(s);
            }
        } catch (IOException e) {
            System.out.println("Ошибка");
        }
        return listLine.toString();
    }

    /**
     * Возвращает мультистроку без лишних комментариев
     *
     * @param listLine мультистрока
     * @return мультистроку без лишних комментариев
     */
    private static String deleteComments(String listLine) {
        return listLine.replaceAll("(/\\*.+?\\*/)|(//.+)[:;a-zA-Zа-яА-ЯЁё]*", "");
    }

    /**
     * Возвращает мульистроку без лишних пробелов
     *
     * @param listLine мультистрока без комментариев
     * @return мультистроку без лишних пробелов
     */
    private static String deleteGamps(String listLine) {
        return listLine.replaceAll("\\s+(?![^\\d\\s])", " ");
    }

    /**
     * Возвращает полученное имя из пути файла без расширения
     *
     * @param soursPath путь файла
     * @return имя файла без расширения
     */
    private static String getFileName(String soursPath) {
        Path name = Paths.get(soursPath);
        String fileName = name.getFileName().toString();
        return fileName.replaceAll("\\..*", "");
    }

    /**
     * Возвращает измененное имя файла в пути
     *
     * @param soursPath путь к файлу
     * @param targetPath   имя файла
     * @return путь с новым именем файла
     */
    private static String newFilePath(String soursPath, String targetPath) {
        return soursPath.replaceAll(targetPath, "\\New" + targetPath);
    }

    /**
     * Записывает мультистроку в файл
     *
     * @param renameMultiply обфусцированная мультистрока
     * @param newFileFullWay путь к файлу с новым именем
     * @throws IOException ошибку
     */
    private static void fileRecorder(String renameMultiply, String newFileFullWay) throws IOException {
        try (FileWriter writer = new FileWriter(newFileFullWay)) {
            writer.write(renameMultiply);
        }
    }

    /**
     * Выводит мультистроку в консоль
     *
     * @param listLine мультистрока
     */
    private static void printOut(String listLine) {
        System.out.println(listLine);
    }
}
